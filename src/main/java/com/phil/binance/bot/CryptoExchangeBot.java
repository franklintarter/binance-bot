package com.phil.binance.bot;

import com.phil.binance.bot.engine.TradingEngine;
import com.phil.binance.bot.exceptions.MalformedStrategyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class CryptoExchangeBot {

  private final TradingEngine tradingEngine;

  @Autowired
  public CryptoExchangeBot(TradingEngine tradingEngine) {
    this.tradingEngine = tradingEngine;
    run();
  }

  public static void main(String[] args) {
    SpringApplication.run(CryptoExchangeBot.class, args);
  }

  private void run() {
    try {
      tradingEngine.run();
    } catch (MalformedStrategyException mse) {
      log.error("MalformedStrategyException caught:", mse);
    } catch (InterruptedException ie) {
      log.error("InterruptedException caught:", ie);
    } finally {
      System.exit(0);
    }
  }
}
