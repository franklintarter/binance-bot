package com.phil.binance.bot.client;

import com.phil.binance.bot.config.BinanceConfig;
import com.phil.binance.bot.enums.Endpoint;
import com.phil.binance.bot.enums.Interval;
import com.phil.binance.bot.mapper.CandleStickMapper;
import com.phil.binance.bot.response.average.AveragePrice;
import com.phil.binance.bot.response.klines.CandleStick;
import com.phil.binance.bot.response.orders.OrderBookResponse;
import com.phil.binance.bot.response.tickers.PriceTicker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Slf4j
@Component
public class PublicClient {

  private BinanceConfig binanceConfig;
  private WebClient webClient;

  @Autowired
  public PublicClient(BinanceConfig binanceConfig) {
    this.binanceConfig = binanceConfig;

    initializeWebClient();
  }

  private void initializeWebClient() {
    this.webClient =
        WebClient.builder()
            .baseUrl(binanceConfig.getUrl())
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .build();
  }

  /**
   * /api/v3/depth
   *
   * @param tradingPair
   * @param limit
   * @return
   */
  public OrderBookResponse marketOrderBook(String tradingPair, int limit) {
    String uri =
        UriComponentsBuilder.newInstance()
            .path(Endpoint.ORDER_BOOK.getUri())
            .queryParam("symbol", tradingPair)
            .queryParam("limit", limit)
            .build()
            .encode()
            .toUriString();

    return webClient.get().uri(uri).retrieve().bodyToMono(OrderBookResponse.class).block();
  }

  /**
   * /api/v3/klines
   *
   * @param tradingPair
   * @param interval
   * @param limit
   * @return
   */
  public List<CandleStick> getCandleSticks(
      String tradingPair, int timeInterval, Interval interval, int limit) {
    String uri =
        UriComponentsBuilder.newInstance()
            .path(Endpoint.CANDLE_STICK.getUri())
            .queryParam("symbol", tradingPair)
            .queryParam("interval", timeInterval + interval.getShortName())
            .queryParam("limit", limit)
            .build()
            .encode()
            .toUriString();

    CandleStickMapper mapper = new CandleStickMapper();
    Object[] candleStickEntries =
        webClient.get().uri(uri).retrieve().bodyToMono(Object[].class).block();

    return mapper.map(candleStickEntries);
  }

  /**
   * /api/v3/avgPrice
   *
   * @return
   */
  public AveragePrice getAveragePrice(String tradingPair) {
    String uri =
        UriComponentsBuilder.newInstance()
            .path(Endpoint.AVERAGE_PRICE.getUri())
            .queryParam("symbol", tradingPair)
            .build()
            .encode()
            .toUriString();

    return webClient.get().uri(uri).retrieve().bodyToMono(AveragePrice.class).block();
  }

  /**
   * /api/v3/ticker/price
   *
   * @return
   */
  public PriceTicker getPriceTicker(String tradingPair) {
    String uri =
        UriComponentsBuilder.newInstance()
            .path(Endpoint.PRICE_TICKER.getUri())
            .queryParam("symbol", tradingPair)
            .build()
            .encode()
            .toUriString();

    return webClient.get().uri(uri).retrieve().bodyToMono(PriceTicker.class).block();
  }
}
