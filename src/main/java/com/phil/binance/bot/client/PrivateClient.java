package com.phil.binance.bot.client;

import com.phil.binance.bot.config.BinanceConfig;
import com.phil.binance.bot.enums.Endpoint;
import com.phil.binance.bot.enums.Side;
import com.phil.binance.bot.enums.Type;
import com.phil.binance.bot.response.acount.Account;
import com.phil.binance.bot.response.orders.NewOrderResponse;
import com.phil.binance.bot.response.orders.Order;
import com.phil.binance.bot.response.trades.Trade;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Slf4j
@Component
public class PrivateClient {

  private static final String ENCRYPTION_ALGORITHM = "HmacSHA256";
  private static final String API_KEY_HEADER = "X-MBX-APIKEY";
  private static final String RECEIVE_WINDOW = "60000";

  private Mac mac;
  private WebClient webClient;
  private BinanceConfig binanceConfig;

  @Autowired
  public PrivateClient(BinanceConfig binanceConfig) {
    this.binanceConfig = binanceConfig;

    initializeWebClient();
    initSecureMessageLayer();
  }

  private void initializeWebClient() {
    this.webClient =
        WebClient.builder()
            .baseUrl(binanceConfig.getUrl())
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .build();
  }

  /**
   * Initialises the secure messaging layer. Sets up the MAC to safeguard the data we send to the
   * exchange. Used to encrypt the hash of the entire message with the private key to ensure message
   * integrity. We fail hard n fast if any of this stuff blows.
   */
  private void initSecureMessageLayer() {
    try {
      mac = Mac.getInstance(ENCRYPTION_ALGORITHM);

      byte[] secretBytes = binanceConfig.getApiSecret().getBytes(StandardCharsets.UTF_8);
      final SecretKeySpec keySpec = new SecretKeySpec(secretBytes, ENCRYPTION_ALGORITHM);

      mac.init(keySpec);

    } catch (NoSuchAlgorithmException e) {
      String errorMsg = "Failed to setup MAC security. HINT: Is HMAC-SHA256 installed?";
      log.error(errorMsg, e);
      throw new IllegalStateException(errorMsg, e);
    } catch (InvalidKeyException e) {
      String errorMsg = "Failed to setup MAC security. Secret key seems invalid!";
      log.error(errorMsg, e);
      throw new IllegalArgumentException(errorMsg, e);
    }
  }

  /**
   * /api/v3/order
   *
   * @param tradingPair
   * @param side
   * @param type
   * @param quantity
   */
  public NewOrderResponse newOrder(String tradingPair, Side side, Type type, BigDecimal quantity) {
    try {
      UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance();

      String queryParams =
          uriBuilder
              .queryParam("symbol", tradingPair)
              .queryParam("side", side.name())
              .queryParam("type", type.name())
              .queryParam("quantity", quantity.toString())
              .queryParam("timestamp", System.currentTimeMillis())
              .queryParam("recvWindow", RECEIVE_WINDOW)
              .build()
              .toUriString()
              .substring(1);

      String totalUri =
          uriBuilder
              .path(Endpoint.ORDER.getUri())
              .queryParam("signature", generateSignature(queryParams))
              .build()
              .toUriString();

      NewOrderResponse result =
          webClient
              .post()
              .uri(totalUri)
              .header(API_KEY_HEADER, binanceConfig.getApiKey())
              .retrieve()
              .bodyToMono(NewOrderResponse.class)
              .block();

      log.info("Received PROD order response: {}", result);
      return result;
    } catch (Exception e) {
      log.error("Exception caught", e);
    }

    return null;
  }

  /**
   * /api/v3/order/test
   *
   * @param tradingPair
   * @param side
   * @param type
   * @param quantity
   */
  public void newTestOrder(String tradingPair, Side side, Type type, BigDecimal quantity) {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance();

    String queryParams =
        uriBuilder
            .queryParam("symbol", tradingPair)
            .queryParam("side", side.name())
            .queryParam("type", type.name())
            .queryParam("quantity", quantity.toString())
            .queryParam("timestamp", System.currentTimeMillis())
            .queryParam("recvWindow", RECEIVE_WINDOW)
            .build()
            .toUriString()
            .substring(1);

    String totalUri =
        uriBuilder
            .path(Endpoint.TEST_ORDER.getUri())
            .queryParam("signature", generateSignature(queryParams))
            .build()
            .toUriString();

    String result =
        webClient
            .post()
            .uri(totalUri)
            .header(API_KEY_HEADER, binanceConfig.getApiKey())
            .retrieve()
            .bodyToMono(String.class)
            .block();

    System.out.println(result);
  }

  public List<Order> getOpenOrders(String symbol) {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance();

    String queryParams =
        uriBuilder
            .queryParam("symbol", symbol)
            .queryParam("timestamp", System.currentTimeMillis())
            .queryParam("recvWindow", "60000")
            .build()
            .toUriString()
            .substring(1);

    String totalUri =
        uriBuilder
            .path(Endpoint.OPEN_ORDERS.getUri())
            .queryParam("signature", generateSignature(queryParams))
            .build()
            .toUriString();

    List<Order> result =
        webClient
            .get()
            .uri(totalUri)
            .header(API_KEY_HEADER, binanceConfig.getApiKey())
            .retrieve()
            .bodyToFlux(Order.class)
            .collectList()
            .block();

    for (Order curOrder : result) {
      log.info(
          "Open order id: {} symbol: {} status: {} original quantity: {}",
          curOrder.getOrderId(),
          curOrder.getSymbol(),
          curOrder.getStatus(),
          curOrder.getOrigQty());
    }

    return result;
  }

  /**
   * /api/v3/allOrders
   *
   * @param symbol
   * @return
   */
  public Order getLatestOrder(String symbol) {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance();

    String queryParams =
        uriBuilder
            .queryParam("symbol", symbol)
            .queryParam("limit", 1)
            .queryParam("timestamp", System.currentTimeMillis())
            .queryParam("recvWindow", "60000")
            .build()
            .toUriString()
            .substring(1);

    String totalUri =
        uriBuilder
            .path(Endpoint.ALL_ORDERS.getUri())
            .queryParam("signature", generateSignature(queryParams))
            .build()
            .toUriString();

    List<Order> result =
        webClient
            .get()
            .uri(totalUri)
            .header(API_KEY_HEADER, binanceConfig.getApiKey())
            .retrieve()
            .bodyToFlux(Order.class)
            .collectList()
            .block();

    return result.get(0);
  }

  /**
   * /api/v3/allOrders
   *
   * @param symbol
   * @return
   */
  public Trade getLatestTrade(String symbol) {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance();

    String queryParams =
        uriBuilder
            .queryParam("symbol", symbol)
            .queryParam("limit", 1)
            .queryParam("timestamp", System.currentTimeMillis())
            .queryParam("recvWindow", "60000")
            .build()
            .toUriString()
            .substring(1);

    String totalUri =
        uriBuilder
            .path(Endpoint.MY_TRADES.getUri())
            .queryParam("signature", generateSignature(queryParams))
            .build()
            .toUriString();

    List<Trade> result =
        webClient
            .get()
            .uri(totalUri)
            .header(API_KEY_HEADER, binanceConfig.getApiKey())
            .retrieve()
            .bodyToFlux(Trade.class)
            .collectList()
            .block();

    return result.get(0);
  }

  /**
   * /api/v3/account
   *
   * @return
   */
  public Account getAccountInformation() {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance();

    String queryParams =
        uriBuilder
            .queryParam("timestamp", System.currentTimeMillis())
            .queryParam("recvWindow", "60000")
            .build()
            .toUriString()
            .substring(1);

    String totalUri =
        uriBuilder
            .path(Endpoint.ACCOUNT.getUri())
            .queryParam("signature", generateSignature(queryParams))
            .build()
            .toUriString();

    Account result =
        webClient
            .get()
            .uri(totalUri)
            .header(API_KEY_HEADER, binanceConfig.getApiKey())
            .retrieve()
            .bodyToMono(Account.class)
            .block();

    return result;
  }

  private String generateSignature(String data) {
    return Hex.encodeHexString(mac.doFinal(data.getBytes(StandardCharsets.UTF_8)));
  }
}
