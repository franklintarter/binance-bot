package com.phil.binance.bot.enums;

import lombok.Getter;

@Getter
public enum Side {

	BUY,
	SELL;

}

