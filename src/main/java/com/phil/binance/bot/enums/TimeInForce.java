package com.phil.binance.bot.enums;

public enum TimeInForce {

	GTC, //good til cancelled
	IOC, //immediate or cancelled
	FOK; //fill or kill
}
