package com.phil.binance.bot.enums;

public enum OrderStatus {

	NEW,
	PARTIALLY_FILLED,
	FILLED,
	CANCELED,
	PENDING_CANCEL,
	REJECTED,
	EXPIRED,
	MARKET;
}
