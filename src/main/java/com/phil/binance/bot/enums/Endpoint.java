package com.phil.binance.bot.enums;

import lombok.Getter;

@Getter
public enum Endpoint {
  TEST_ORDER("/api/v3/order/test"),
  ORDER("/api/v3/order"),
  ORDER_BOOK("/api/v3/depth"),
  CANDLE_STICK("/api/v3/klines"),
  OPEN_ORDERS("/api/v3/openOrders"),
  ALL_ORDERS("/api/v3/allOrders"),
  MY_TRADES("/api/v3/myTrades"),
  ACCOUNT("/api/v3/account"),
  AVERAGE_PRICE("/api/v3/avgPrice"),
  PRICE_TICKER("/api/v3/ticker/price");

  private final String uri;

  Endpoint(String uri) {
    this.uri = uri;
  }
}
