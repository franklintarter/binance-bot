package com.phil.binance.bot.strategies;

import com.phil.binance.bot.client.PrivateClient;
import com.phil.binance.bot.client.PublicClient;
import com.phil.binance.bot.config.EngineConfig;
import com.phil.binance.bot.config.StrategyConfig;
import com.phil.binance.bot.enums.Interval;
import com.phil.binance.bot.enums.Side;
import com.phil.binance.bot.response.acount.Account;
import com.phil.binance.bot.response.acount.Balance;
import com.phil.binance.bot.response.klines.CandleStick;
import com.phil.binance.bot.response.orders.Order;
import com.phil.binance.bot.response.tickers.PriceTicker;
import com.phil.binance.bot.response.trades.Trade;
import com.phil.binance.bot.util.BalanceFinder;
import com.phil.binance.bot.util.TradingPair;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
@Service
public class SimpleMovingAverage extends Strategy {

  private final PrivateClient privateClient;
  private final PublicClient publicClient;
  private final EngineConfig engineConfig;
  private final StrategyConfig strategyConfig;

  public SimpleMovingAverage(
      PrivateClient privateClient,
      PublicClient publicClient,
      EngineConfig engineConfig,
      StrategyConfig strategyConfig) {
    super(privateClient, engineConfig);
    this.privateClient = privateClient;
    this.publicClient = publicClient;
    this.engineConfig = engineConfig;
    this.strategyConfig = strategyConfig;
  }

  public boolean isEnabled() {
    return strategyConfig.isSma();
  }

  public void execute() throws InterruptedException {
    BigDecimal latestAverage = new BigDecimal("0.00");
    Pair<String, String> leftRightPair = TradingPair.split(engineConfig.getTradingPair());
    int executionNum = 1;

    while (true) {
      List<CandleStick> candles =
          publicClient.getCandleSticks(
              engineConfig.getTradingPair(), strategyConfig.getMinuteInterval(),
              Interval.MINUTES, strategyConfig.getSmaWindow());

      // Σ
      for (CandleStick curCandle : candles) {
        latestAverage = curCandle.getClose().add(latestAverage);
      }

      latestAverage = latestAverage.divide(
          new BigDecimal(strategyConfig.getSmaWindow()), 2, RoundingMode.HALF_DOWN);

      Account account = privateClient.getAccountInformation();

      Balance purchasingBalance = BalanceFinder.find(leftRightPair.getValue1(), account.getBalances());
      BigDecimal availablePurchasingPower = new BigDecimal(purchasingBalance.getFree());

      PriceTicker priceTicker = publicClient.getPriceTicker(engineConfig.getTradingPair());
      BigDecimal currentTradingPairPrice = new BigDecimal(priceTicker.getPrice());

      Order latestOrder = privateClient.getLatestOrder(engineConfig.getTradingPair());
      Trade latestTrade = privateClient.getLatestTrade(engineConfig.getTradingPair());

      BigDecimal quantityPotential =
          availablePurchasingPower.divide(currentTradingPairPrice, 2, RoundingMode.HALF_DOWN);

      //-1% quantity potential to have available balance due to fast market movements
      quantityPotential = quantityPotential.subtract(quantityPotential.multiply(new BigDecimal("0.01")));

      log.info("================ {} {} ================", engineConfig.getTradingPair(), executionNum);
      log.info("available balance in {}: {}", leftRightPair.getValue1(), availablePurchasingPower);
      log.info("current trading pair price: {}", currentTradingPairPrice);
      log.info("latest average trading pair price: {}", latestAverage);
      log.info("quantity potential: {}", quantityPotential);
      log.info("last order side: {}", latestOrder.getSide());
      log.info("last order price: {}", latestTrade.getPrice());
      log.info("last order {} quantity: {}", leftRightPair.getValue0(), latestOrder.getOrigQty());
      log.info("last order {} quantity: {}", leftRightPair.getValue1(), latestOrder.getCummulativeQuoteQty());
      log.info("===========================================");

      if (latestOrder.getSide().equals("SELL")
          && latestOrder.getSymbol().equals(engineConfig.getTradingPair())
          && account.isCanTrade()
          && currentTradingPairPrice.compareTo(latestAverage) < 0) {

        log.info("BUY signal found! Purchase {} with {} funds", quantityPotential, availablePurchasingPower);
        log.info("===========================================");
        placeMarketOrder(Side.BUY, quantityPotential);
      } else if (latestOrder.getSide().equals("BUY")
          && latestOrder.getSymbol().equals(engineConfig.getTradingPair())) {

        BigDecimal targetPrice = new BigDecimal(latestTrade.getPrice()).multiply(new BigDecimal(engineConfig.getProfitMargin()));
        log.info("Looking to SELL at {}. Current price is {}", targetPrice, currentTradingPairPrice);

        if (currentTradingPairPrice.compareTo(targetPrice) > 0) {
          Balance leftCurrencyBalance = BalanceFinder.find(leftRightPair.getValue0(), account.getBalances());
          log.info("SELL signal found! Selling {} {} at {}", leftCurrencyBalance.getFree(), engineConfig.getTradingPair(), currentTradingPairPrice);
          log.info("===========================================");
          placeMarketOrder(Side.SELL, new BigDecimal(leftCurrencyBalance.getFree()));
        } else {
          log.info("No SELL signal found... Continue.");
        }
      }

      executionNum++;
      log.info("Sleeping for {} ms", engineConfig.getSleepIntervalMs());
      Thread.sleep(engineConfig.getSleepIntervalMs());
    }
  }
}
