package com.phil.binance.bot.strategies;

import com.phil.binance.bot.client.PrivateClient;
import com.phil.binance.bot.config.EngineConfig;
import com.phil.binance.bot.enums.Side;
import com.phil.binance.bot.enums.Type;
import com.phil.binance.bot.response.orders.NewOrderResponse;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public abstract class Strategy {

	private PrivateClient privateClient;
	private EngineConfig engineConfig;

	public Strategy(PrivateClient privateClient, EngineConfig engineConfig) {
		this.privateClient = privateClient;
		this.engineConfig = engineConfig;
	}

	public abstract void execute() throws InterruptedException;

	public abstract boolean isEnabled();

	protected void placeMarketOrder(Side buyOrSell, BigDecimal quantity) {
		switch (engineConfig.getEnvironment()) {
			case "dev":
				log.info("Creating new TEST order...");
				privateClient.newTestOrder(engineConfig.getTradingPair(), buyOrSell, Type.MARKET, quantity);

				break;
			case "prod":
				log.info("Creating new PROD order...");
				NewOrderResponse response = privateClient.newOrder(engineConfig.getTradingPair(), buyOrSell, Type.MARKET, quantity);

				log.info("Binance market order response {}", response);
				break;
			default:
				log.error("Environment not set. Must be either 'dev' or 'prod");
				System.exit(-1);
		}
	}
}
