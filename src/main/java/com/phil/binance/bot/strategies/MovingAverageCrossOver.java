package com.phil.binance.bot.strategies;

import com.phil.binance.bot.client.PrivateClient;
import com.phil.binance.bot.client.PublicClient;
import com.phil.binance.bot.config.EngineConfig;
import com.phil.binance.bot.config.StrategyConfig;
import com.phil.binance.bot.enums.Interval;
import com.phil.binance.bot.enums.Side;
import com.phil.binance.bot.response.acount.Account;
import com.phil.binance.bot.response.acount.Balance;
import com.phil.binance.bot.response.klines.CandleStick;
import com.phil.binance.bot.response.orders.Order;
import com.phil.binance.bot.response.orders.OrderBookResponse;
import com.phil.binance.bot.response.tickers.PriceTicker;
import com.phil.binance.bot.response.trades.Trade;
import com.phil.binance.bot.util.BalanceFinder;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MovingAverageCrossOver extends Strategy {

  private final PrivateClient privateClient;
  private final PublicClient publicClient;
  private final EngineConfig engineConfig;
  private final StrategyConfig strategyConfig;
  private final ArrayList<Pair<BigDecimal, BigDecimal>> movingAverages;

  @Autowired
  public MovingAverageCrossOver(
      PrivateClient privateClient,
      PublicClient publicClient,
      EngineConfig engineConfig,
      StrategyConfig strategyConfig) {
    super(privateClient, engineConfig);
    this.privateClient = privateClient;
    this.publicClient = publicClient;
    this.engineConfig = engineConfig;
    this.strategyConfig = strategyConfig;
    this.movingAverages = new ArrayList<>();
  }

  public boolean isEnabled() {
    return strategyConfig.isMaco();
  }

  public void execute() throws InterruptedException {
    while (true) {
      List<CandleStick> longWindowCandles =
          publicClient.getCandleSticks(
              engineConfig.getTradingPair(), 1, Interval.MINUTES, strategyConfig.getSmaWindow());

      BigDecimal averagePrice = new BigDecimal(0);
      for (CandleStick curCandlestick : longWindowCandles) {
        averagePrice = averagePrice.add(curCandlestick.getClose());
      }
      averagePrice =
          averagePrice.divide(
              new BigDecimal(strategyConfig.getSmaWindow()), 2, RoundingMode.HALF_DOWN);

      PriceTicker priceTicker = publicClient.getPriceTicker(engineConfig.getTradingPair());
      log.info("average: {} current price: {}", averagePrice, priceTicker.getPrice());

      Account account = privateClient.getAccountInformation();

      Balance usdBalance = BalanceFinder.find("USD", account.getBalances());
      BigDecimal availableUSD = new BigDecimal(usdBalance.getFree());
      BigDecimal currentPrice = new BigDecimal(priceTicker.getPrice());

      log.info("available USD: {}", availableUSD);
      log.info("current price: {}", currentPrice);

      Order latestOrder = privateClient.getLatestOrder(engineConfig.getTradingPair());
      Trade latestTrade = privateClient.getLatestTrade(engineConfig.getTradingPair());
      log.info("latest order: {}", latestOrder);
      log.info("latest trade: {}", latestTrade);

      if (latestOrder.getSide().equals("SELL")
          && latestOrder.getSymbol().equals(engineConfig.getTradingPair())
          && account.isCanTrade()
          && availableUSD.compareTo(new BigDecimal(10))
              > 0 // Check that we have more than $10 to buy with
          && currentPrice.compareTo(averagePrice) < 0) {

        BigDecimal quantityPotential = availableUSD.divide(currentPrice, RoundingMode.HALF_DOWN);

        log.info(
            ">>>>> BUY signal found! Purchase {} with {} funds", quantityPotential, availableUSD);
        placeMarketOrder(Side.BUY, quantityPotential);
      } else if (latestOrder.getSide().equals("BUY") && latestOrder.getSymbol().equals(engineConfig.getTradingPair())) {
        BigDecimal targetPrice = new BigDecimal(latestTrade.getPrice()).multiply(new BigDecimal(1.01));
        log.info("Looking to SELL at {} current price is {}", targetPrice, currentPrice);

        if (currentPrice.compareTo(targetPrice) > 0) {
          Balance btcBalance = BalanceFinder.find("BTC", account.getBalances());
          log.info(
              ">>>>> SELL signal found! Selling {} BTC at {}", btcBalance.getFree(), currentPrice);
          placeMarketOrder(Side.SELL, new BigDecimal(btcBalance.getFree()));
        } else {
          log.info("No SELL signal found... Continue.");
        }
      }

      // FIND SIGNAL TO BUY OR SELL
      OrderBookResponse response = publicClient.marketOrderBook(engineConfig.getTradingPair(), 100);
      Pair<BigDecimal, BigDecimal> shortLongAverage = calculateSimpleShortLongMovingAverage();

      if (movingAverages.size() < 100) {
        movingAverages.add(shortLongAverage);
      } else {
        movingAverages.remove(movingAverages.size() - 1); // Remove last item
      }

      log.info("All moving averages: {}", movingAverages);

      // Determine cross over signal
      // shorter-term MA cross BELOW longer-term MA is SELL signal
      // shorter-term MA cross ABOVE longer-term MA is BUY signal
      int isCrossed = determineMovingAverageCross();

      List<Order> orders = privateClient.getOpenOrders(engineConfig.getTradingPair());
      if (!orders.isEmpty() && isCrossed == -1) {
        log.info("SELLING current position");
        placeMarketOrder(Side.SELL, new BigDecimal("0.003"));
      } else if (orders.isEmpty() && isCrossed == 1) {
        log.info("BUYING position");
        placeMarketOrder(Side.BUY, new BigDecimal("0.003"));
      }

      log.info("Sleeping for {} ms", engineConfig.getSleepIntervalMs());
      Thread.sleep(engineConfig.getSleepIntervalMs());
    }
  }

  /**
   * SELL signal: -1 is short MA crossing BELOW long MA BUY signal: 1 is short MA crossing ABOVE
   * long MA
   *
   * @return
   */
  private int determineMovingAverageCross() {
    int previousTrend = 0;

    for (int i = 0; i < movingAverages.size() - 1; i++) {
      BigDecimal currentShort = movingAverages.get(i).getValue0();
      BigDecimal nextLong = movingAverages.get(i + 1).getValue1();

      log.info("Comparing Short MA: {} to Long MA:{}", currentShort, nextLong);
      int currentTrend = currentShort.compareTo(nextLong);

      if (previousTrend == 1 && currentTrend == -1) {
        // This is a SELL signal
        log.info("!!! Found SELL signal");
        return -1;
      } else if (previousTrend == -1 && currentTrend == 1) {
        // This is a BUY signal
        log.info("!!! Found BUY signal");
        return 1;
      }

      previousTrend = currentShort.compareTo(nextLong);
    }

    log.info("No cross found. Continue");
    return 0;
  }

  /**
   * Left value is SHORT moving average Right value is LONG moving average
   *
   * @return
   */
  private Pair<BigDecimal, BigDecimal> calculateSimpleShortLongMovingAverage() {
    List<CandleStick> longWindowCandles =
        publicClient.getCandleSticks(
            engineConfig.getTradingPair(), 4, Interval.HOURS, strategyConfig.getLongWindow());

    BigDecimal longAverage = new BigDecimal(0.00);
    for (CandleStick curCandlestick : longWindowCandles) {
      longAverage = curCandlestick.getClose().add(longAverage);
    }

    longAverage = longAverage.divide(new BigDecimal(strategyConfig.getLongWindow()));

    List<CandleStick> shortWindowCandles =
        publicClient.getCandleSticks(
            engineConfig.getTradingPair(), 4, Interval.HOURS, strategyConfig.getShortWindow());

    BigDecimal shortAverage = new BigDecimal(0.00);
    for (CandleStick curCandlestick : shortWindowCandles) {
      shortAverage = curCandlestick.getClose().add(shortAverage);
    }

    shortAverage = shortAverage.divide(new BigDecimal(strategyConfig.getShortWindow()));

    log.info("Short MA: {}, Long MA: {}", shortAverage, longAverage);
    return new Pair<>(shortAverage, longAverage);
  }
}
