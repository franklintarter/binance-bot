package com.phil.binance.bot.mapper;

import com.phil.binance.bot.response.klines.CandleStick;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CandleStickMapper {

  /**
   * [ [ 1499040000000, // Open time "0.01634790", // Open "0.80000000", // High "0.01575800", //
   * Low "0.01577100", // Close "148976.11427815", // Volume 1499644799999, // Close time
   * "2434.19055334", // Quote asset volume 308, // Number of trades "1756.87402397", // Taker buy
   * base asset volume "28.46694368", // Taker buy quote asset volume "17928899.62484339" // Ignore.
   * ] ]
   *
   * @param rawCandleStickEntries
   * @return
   */
  public List<CandleStick> map(Object[] rawCandleStickEntries) {
    List<CandleStick> candleSticks = new ArrayList<>();

    for (Object curObj : rawCandleStickEntries) {
      Object[] objParts = curObj.toString().replaceAll("\\[", "").replaceAll("\\]", "").split(",");

      CandleStick curCandleStick = new CandleStick();
      curCandleStick.setOpenTime(Long.parseLong(objParts[0].toString().trim()));
      curCandleStick.setOpen(new BigDecimal(objParts[1].toString().trim()));
      curCandleStick.setHigh(new BigDecimal(objParts[2].toString().trim()));
      curCandleStick.setLow(new BigDecimal(objParts[3].toString().trim()));
      curCandleStick.setClose(new BigDecimal(objParts[4].toString().trim()));
      curCandleStick.setVolume(new BigDecimal(objParts[5].toString().trim()));
      curCandleStick.setCloseTime(Long.parseLong(objParts[6].toString().trim()));
      curCandleStick.setQuoteAssetVolume(new BigDecimal(objParts[7].toString().trim()));
      curCandleStick.setNumTrades(Long.parseLong(objParts[8].toString().trim()));
      curCandleStick.setTakerBuyBaseVolume(new BigDecimal(objParts[9].toString().trim()));
      curCandleStick.setTakerBuyQuoteVolume(new BigDecimal(objParts[10].toString().trim()));

      candleSticks.add(curCandleStick);
    }

    return candleSticks;
  }
}
