package com.phil.binance.bot.response.orders;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OrderFill {

  private String price;
  private String quantity;
  private String commission;
  private String commissionAsset;
}
