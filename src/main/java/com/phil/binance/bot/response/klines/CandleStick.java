package com.phil.binance.bot.response.klines;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class CandleStick {

  private long openTime;
  private long closeTime;
  private BigDecimal open;
  private BigDecimal high;
  private BigDecimal low;
  private BigDecimal close;
  private BigDecimal volume;
  private BigDecimal quoteAssetVolume;
  private long numTrades;
  private BigDecimal takerBuyBaseVolume;
  private BigDecimal takerBuyQuoteVolume;
}
