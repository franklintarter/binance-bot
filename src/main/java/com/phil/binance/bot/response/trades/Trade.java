package com.phil.binance.bot.response.trades;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Trade {

	private String symbol;
	private long id;
	private long orderId;
	private long orderListId;
	private String price;
	private String qty;
	private String quoteQty;
	private String commission;
	private String commissionAsset;
	private long time;
	private boolean isBuyer;
	private boolean isMaker;
	private boolean isBestMatch;
}
