package com.phil.binance.bot.response.average;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AveragePrice {

	private int mins;
	private String price;
}
