package com.phil.binance.bot.engine;

import com.phil.binance.bot.exceptions.MalformedStrategyException;
import com.phil.binance.bot.strategies.Strategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TradingEngine {

  private final List<Strategy> strategies;

  @Autowired
  public TradingEngine(List<Strategy> strategies) {
    this.strategies = strategies;
  }

  public void run() throws MalformedStrategyException, InterruptedException {
    Strategy runningStrategy = null;
    for (Strategy curStrategy : strategies) {
      if (curStrategy.isEnabled()) {
        runningStrategy = curStrategy;
        break;
      }
    }

    if (runningStrategy == null) {
      throw new MalformedStrategyException("Must configure strategy in properties file.");
    }

    runningStrategy.execute();
  }
}
