package com.phil.binance.bot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class EngineConfig {

  @Value("${spring.profiles.active}")
  private String environment;

  @Value("${engine.sleep-interval-ms:60000}")
  private int sleepIntervalMs;

  @Value("${engine.trading-volume}")
  private String tradingVolume;

  @Value("${engine.trading-pair}")
  private String tradingPair;

  @Value("${engine.profit-margin}")
  private String profitMargin;
}
