package com.phil.binance.bot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class StrategyConfig {

	@Value("${strategy.maco.enabled:false}")
	private boolean maco;

	@Value("${strategy.maco.short-window:2}")
	private int shortWindow;

	@Value("${strategy.maco.long-window:20}")
	private int longWindow;

	@Value("${strategy.sma.enabled:false}")
	private boolean sma;

	@Value("${strategy.sma.window:500}")
	private int smaWindow;

	@Value("${strategy.sma.interval-minute:1}")
	private int minuteInterval;
}
