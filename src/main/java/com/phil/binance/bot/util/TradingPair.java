package com.phil.binance.bot.util;

import org.javatuples.Pair;

public class TradingPair {

	/*
	 * ETCUSD
	 * Left currency is what we want to buy
	 * Right currency is what we buy with
	 */
	public static Pair<String, String> split(String rawTradingPair) {
		return new Pair<>(rawTradingPair.substring(0,3), rawTradingPair.substring(3,6));
	}
}
